"""
api_wrapper/tests/base.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Project Status

Wrapper to interface with REST clients and map responses into our models.
"""

from django.test import TestCase
from django.conf import settings
from django.contrib.auth import get_user_model
from django.test.utils import override_settings

from mock import patch

# from harvest import HarvestClient

User = get_user_model()
