"""
api_wrapper/tests/test_harvest_api.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

import sys
import os
from subprocess import check_call

import pytest
from model_mommy import mommy

from harvest_api import HarvestApi, harvest_client
from harvest_api.models import *

from .base import *
from harvest_api.settings import HARVEST_API_SETTINGS


class HarvestApiConfigTest(TestCase):

    def test_api_settings(self):
        self.assertIsNotNone(HARVEST_API_SETTINGS)

    def test_project_setting(self):
        if HARVEST_API_SETTINGS.get('project'):
            self.assertTrue(hasattr(HarvestProject, 'project'))
        else:
            self.assertFalse(hasattr(HarvestProject, 'project'))


@pytest.mark.django_db
class TestHarvestApi(TestCase):

    def setUp(self):
        self.user = mommy.make('User', username='admin')
        harvestuser = mommy.make('HarvestUser', user=self.user)
        self.access_token = self.user.harvestuser.access_token
        self.api = HarvestApi(harvest_client, self.access_token)

    def test_init(self):
        self.assertEqual(self.access_token, self.api.access_token)
        self.assertEqual(self.access_token, self.api.access_params.get('access_token'))

    def test_load_projects(self):
        mock_rsp = [
            {
                "project": {
                    "created_at": "2015-08-11T21:14:44Z",
                    "hint_latest_record_at": None,
                    "estimate_by": "project",
                    "starts_on": "2015-08-11",
                    "id": 8658454,
                    "client_id": 3719407,
                    "ends_on": "2016-08-11",
                    "code": "1",
                    "bill_by": "Project",
                    "updated_at": "2015-08-11T21:14:44Z",
                    "budget_by": "project",
                    "active": True,
                    "hint_earliest_record_at": None,
                    "notify_when_over_budget": False,
                    "show_budget_to_all": False,
                    "over_budget_notified_at": None,
                    "over_budget_notification_percentage": 80.0,
                    "billable": True,
                    "hourly_rate": 100.0,
                    "budget": 2000.0,
                    "cost_budget": None,
                    "name": "NoThyme App",
                    "estimate": 2000.0,
                    "notes": "",
                    "cost_budget_include_expenses": False
                }
            },
            {
                "project": {
                    "code": "321",
                    "estimate": None,
                    "notes": "AndroidApp for Needs some Apps",
                    "show_budget_to_all": False,
                    "client_id": 3757925,
                    "over_budget_notification_percentage": 80.0,
                    "created_at": "2015-08-27T00:42:20Z",
                    "ends_on": None,
                    "notify_when_over_budget": True,
                    "hourly_rate": None,
                    "billable": True,
                    "cost_budget": None,
                    "updated_at": "2015-08-27T00:43:31Z",
                    "id": 8759970,
                    "budget": None,
                    "active": True,
                    "hint_latest_record_at": None,
                    "name": "AndroidApp",
                    "cost_budget_include_expenses": False,
                    "bill_by": "Tasks",
                    "over_budget_notified_at": None,
                    "estimate_by": "None",
                    "budget_by": "None",
                    "hint_earliest_record_at": None,
                    "starts_on": "2015-08-26"
                }
            }
        ]
        client_id = mock_rsp[1].get('project').get('client_id')
        project_id = mock_rsp[1].get('project').get('id')
        client = mommy.make('HarvestClient', harvest_id=client_id)
        with patch.object(harvest_client, 'projects', return_value=mock_rsp) as project_mock:
            self.api.load_projects()    
            self.assertTrue(project_mock.called, 'Load projects method not called.')
            self.assertTrue(isinstance(project_mock.return_value, list), '%s is not a list' % project_mock.return_value)
            project = HarvestProject.objects.get(harvest_id=project_id)
            self.assertTrue(project.client == client, 'Client not set on project.')

    def test_load_clients(self):
        mock_rsp = [
            {
                "client": {
                    "default_invoice_timeframe": None,
                    "highrise_id": None,
                    "created_at": "2015-08-27T00:42:20Z",
                    "updated_at": "2015-08-27T00:42:20Z",
                    "currency_symbol": "$",
                    "last_invoice_kind": None,
                    "currency": "United States Dollar - USD",
                    "name": "NeedsSomeApps",
                    "cache_version": 1443604242,
                    "active": True,
                    "id": 3757925,
                    "details": ""
                }
            }, 
            {
                "client": {
                    "default_invoice_timeframe": None,
                    "highrise_id": None,
                    "created_at": "2015-08-11T21:14:44Z",
                    "updated_at": "2015-08-11T21:14:44Z",
                    "currency_symbol": "$",
                    "last_invoice_kind": None,
                    "currency": "United States Dollar - USD",
                    "name": "NoThyme",
                    "cache_version": 4037683871,
                    "active": True,
                    "id": 3719407,
                    "details": ""
                }
            }
        ]
        with patch.object(harvest_client, 'clients', return_value=mock_rsp) as clients_mock:
            self.api.load_clients()
            self.assertTrue(clients_mock.called, 'List clients method not called.')
            self.assertTrue(isinstance(clients_mock.return_value, list), '%s is not a list' % clients_mock.return_value)
            self.assertTrue(HarvestClient.objects.get(harvest_id=3719407), "Client object not created")

    def test_load_tasks(self):
        mock_rsp = [
            {
                "task": {
                    "id": 4647036,
                    "updated_at": "2015-08-11T21:09:28Z",
                    "deactivated": False,
                    "created_at": "2015-08-11T21:09:28Z",
                    "billable_by_default": False,
                    "name": "Business Development",
                    "is_default": True,
                    "default_hourly_rate": 0
                }
            }, 
            {
                "task": {
                    "id": 4647032,
                    "updated_at": "2015-08-11T21:09:28Z",
                    "deactivated": False,
                    "created_at": "2015-08-11T21:09:28Z",
                    "billable_by_default": True,
                    "name": "Graphic Design",
                    "is_default": True,
                    "default_hourly_rate": 0
                }
            }
        ] 
        with patch.object(harvest_client, 'tasks', return_value=mock_rsp) as tasks_mock:
            self.api.load_tasks()
            self.assertTrue(tasks_mock.called, 'List clients method not called.')
            self.assertTrue(isinstance(tasks_mock.return_value, list), '%s is not a list' % tasks_mock.return_value)
            self.assertTrue(HarvestTask.objects.get(harvest_id=4647032), "Task object not created")

    # def test_load_today_entries(self):
    #     #TODO: Build mock rsp of day entries.
    #     with patch.object(harvest_client, 'get_day', return_value={'day_entries':[]}) as entries_mock:
    #         self.api.load_today()
    #         self.assertTrue(entries_mock.called, 'Today entries method not called.')
    #         self.assertTrue(isinstance(entries_mock.return_value, dict), '%s is not a list' % entries_mock.return_value)
