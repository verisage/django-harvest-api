"""Minimal settings needed for tests."""

import os

def pytest_configure():
    from django.conf import settings

    settings.configure(
        DEBUG=True,
        DATABASES={'default': {'ENGINE': 'django.db.backends.sqlite3',
                               'NAME': 'sqlite.db'}},
        SECRET_KEY='Just a test key',
        STATIC_URL='/static/',
        ROOT_URLCONF='tests.urls',
        INSTALLED_APPS=(
            'django.contrib.admin',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            'harvest_api',
        ),
        HARVEST_DOMAIN='http://divshop.harvestapp.com/',
        HARVEST_CLIENT_ID='Fake id',
        HARVEST_CLIENT_SECRET='Fake secret',
        USE_TZ=True,
        TIME_ZONE='America/Chicago'
    )

    import django
    django.setup()