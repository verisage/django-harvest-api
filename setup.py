from setuptools import setup, find_packages

setup(
    name='django-harvest-api',
    version='0.0.1',
    description='Interface to map in data from Harvest to Django models.',
    # url='https://github.com/pypa/sampleproject',
    author='Jake Harding',
    author_email='jharding@verisage.us',
    license='MIT',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Developers',
        'Topic :: Software Development',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
    ],
    keywords='rest harvest setuptools development',
    packages=find_packages(exclude=['contrib', 'docs', 'tests*', 'example']),
    dependency_links=[
        # 'git+git@github.com:jakeharding/python-harvest.git@bugfix/python3#egg=python_harvest'
    ],
    install_requires=[
        'django', 
        'djangorestframework',
        # 'python-harvest',
        'django-phonenumber-field'
    ],
)