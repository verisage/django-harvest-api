This package is used to retrieve data from Harvest time tracking tool and map it into Django models.

Makes use of the UUIDField in Django 1.8 so Django 1.8 is required.

To run the tests in all environments:

 - clone the repo
 - cd into django-harvest-api
 - run tox


To setup the example project:

 - clone the repo
 - cd into django-harvest-api/example
 - setup settings according to your harvest account
 - make and run migrations
 - create a superuser
 - start the server and login in
 - follow instructions on index page.