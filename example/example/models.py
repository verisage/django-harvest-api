"""
example/models.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Some example models to allow relations to Harvest models.
"""


from django.db import models as m

class Project(m.Model):
    total_hours_worked = m.IntegerField(default=100)

class Client(m.Model):
    is_active = m.BooleanField(default=True)

class Task(m.Model):
    is_active = m.BooleanField(default=True)
