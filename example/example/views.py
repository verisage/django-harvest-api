"""
example/views.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

from datetime import timedelta

from django.shortcuts import render
from django.views.generic import TemplateView
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.conf import settings

from harvest import HOURS_OF_VALID_TOKEN, OauthKey

from harvest_api import HarvestApi, harvest_client, get_harvest_oauth_tokens, refresh_harvest_oauth_tokens


def class_login_required(View):
    View.dispatch = method_decorator(login_required)(View.dispatch)
    return View


@class_login_required
class IndexView(TemplateView):
    template_name = "index.html"

    def get(self, r):
        tokens = None
        if r.GET.get(OauthKey.CODE):
            "User just authorized access to Harvest."
            tokens = get_harvest_oauth_tokens(r.GET.get(OauthKey.CODE))
        elif hasattr(self.request.user, 'harvestuser'): 
            if (timezone.now() - r.user.harvestuser.last_refreshed_token) > timedelta(hours=HOURS_OF_VALID_TOKEN):
                "User has an expired access token."
                tokens = refresh_harvest_oauth_tokens(r.user.refresh_token)
            else:
                "User has a valid token."
                tokens = (r.user.harvestuser.access_token, r.user.harvestuser.refresh_token)

        if tokens:
            api = HarvestApi(harvest_client, tokens)
            api.load_account(r.user, save_refresh=True)
            api.load_clients()
            api.load_projects()

        return super(IndexView, self).get(r)
        
    def get_context_data(self):
        cxt = super(IndexView, self).get_context_data()
        if not hasattr(self.request.user, 'harvestuser'):
            cxt.update({
                'harvest_auth_url': harvest_client.authorize_url 
            })
        else:
            cxt.update({
                'projects': self.request.user.harvestuser.harvest_projects.all()
            })
        return cxt
