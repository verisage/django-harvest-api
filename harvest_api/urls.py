"""
urls.py

Copyright (c) 2015 Verisage, LLC.

Author J. Harding, 2015
"""

from .views import *

from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register(r'harvestproject', HarvestProjectViewSet)

urlpatterns = router.urls