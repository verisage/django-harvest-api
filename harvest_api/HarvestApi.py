"""
api_wrapper/HarvestApi.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Interface for Harvest REST.
"""


from django.utils import timezone

from harvest import OauthKey

from .serializers import *
from .models import HarvestUser


class HarvestApi(object):
    """Class that will use the HarvestClient to retrieve the data and DRF to map them to models."""

    def __init__(self, client, tokens):
        """Constructor. All arguments are required.

        @param: client - The HarvestClient instance to use
        @param: access_token - The users Oauth2 access token.
        """
        self.client = client
        self.access_token = tokens[0]
        self.refresh_token = tokens[1]

        self.access_params = {
            OauthKey.ACCESS_TOKEN: self.access_token
        }

    def load_account(self, user, save_refresh=False):
        account_data = self.client.who_am_i(params=self.access_params)
        if hasattr(user, 'harvestuser'):
            serial = HarvestUserAccountSerializer(user.harvestuser, data=account_data.get('user'))
        else:
            serial = HarvestUserAccountSerializer(data=account_data.get('user'))
        if serial.is_valid(raise_exception=True):
            harvest_user = serial.save()
            harvest_user.user = user
            harvest_user.access_token = self.access_token
            harvest_user.refresh_token = self.refresh_token
            harvest_user.has_auth_harvest = True
            if save_refresh:
                harvest_user.last_refreshed_token = timezone.now()
            harvest_user.save()


    def load_clients(self):
        clients = self.client.clients(params=self.access_params)
        serial = HarvestClientSerializer(data=clients, many=True)
        if serial.is_valid(raise_exception=True):
            serial.save()

    def load_projects(self):
        projects = self.client.projects(params=self.access_params)
        serial = HarvestProjectSerializer(data=projects, many=True)
        if serial.is_valid(raise_exception=True):
            harvest_projects = serial.save()
            harvest_user = HarvestUser.objects.get(
                access_token=self.access_token,
                refresh_token=self.refresh_token
            )
            for p in harvest_projects:
                harvest_user.harvest_projects.add(p)

    def load_tasks(self):
        tasks = self.client.tasks(params=self.access_params)
        serial = HarvestTaskSerializer(data=tasks, many=True)
        if serial.is_valid(raise_exception=True):
            serial.save()

    # def load_day_entries(self, year=None, day=None):
    #     """Retrieves time entries for the specified day or today if day isn't specified.
    #     @param: year - Year for the day requested
    #     @param: day - Day of the year requested, i.e. 1 for January 1st or 365 for Decemeber 31st.
    #     """
    #     if year and day:
    #         entries = self.client.get_day(day_of_the_year=day, year=year, params=self.access_params)
    #     elif not year and day:
    #         year  = timezone.now().year
    #         entries = self.client.get_day(day_of_the_year=day, year=year, params=self.access_params)
    #     else:
    #         entries = self.client.today(params=self.access_params)
    #     serial = DayEntrySerializer(data=entries)
    #     if serial.is_valid(raise_exception=True):
    #         serial.save()

    def load_today(self):
        now = timezone.localtime(timezone.now())
        self.load_day_entries(day=now.timetuple().tm_yday, year=now.year)

    def load_day(self, day, year):
        self.load_day_entries(day_of_the_year=day, year=year)

    def load_data(self):
        self.load_clients()
        self.load_projects()
