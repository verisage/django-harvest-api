"""
settings.py

Copyright (c) 2015 Verisage, LLC.

Author J. Harding, 2015
"""


from django.conf import settings

HARVEST_API_SETTINGS = getattr(settings, 'HARVEST_API_SETTINGS', {})
HARVEST_DOMAIN = getattr(settings, 'HARVEST_DOMAIN', None)
HARVEST_CLIENT_ID = getattr(settings, 'HARVEST_CLIENT_ID', None)
HARVEST_CLIENT_SECRET = getattr(settings, 'HARVEST_CLIENT_SECRET', None)
HARVEST_REDIRECT = getattr(settings, 'HARVEST_REDIRECT', None)


assert HARVEST_DOMAIN, "Harvest domain is needed."
assert HARVEST_CLIENT_ID, "Harvest client id is needed."
assert HARVEST_CLIENT_SECRET, 'Harvest secret is needed.'
