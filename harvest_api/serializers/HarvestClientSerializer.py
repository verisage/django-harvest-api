"""
Clients/serializers/ClientSerializer.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Client Status
"""

from .base import *


class ClientListSerializer(ListSerialMixin, s.ListSerializer):

    DATA_KEY = 'client'


class ClientModelSerializer(s.ModelSerializer):
    id = s.IntegerField(source='harvest_id')

    class Meta:
        model = HarvestClient
        exclude = ['uuid', 'harvest_id']


class HarvestClientSerializer(s.Serializer):

    client = ClientModelSerializer()
    class Meta:
        list_serializer_class = ClientListSerializer