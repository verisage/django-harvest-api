"""
serializers/HarvestAccountSerializer.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

from .base import *
from .HarvestUserSerializer import HarvestUserAccountSerializer


class HarvestAccountSerializer(s.Serializer):
    """Maps in user data from a request to the account."""
    user = HarvestUserAccountSerializer()
