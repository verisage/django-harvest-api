"""
projects/serializers/ProjectSerializer.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Project Status

Holds the serializers needed for deserializing projects from Harvest.
"""


from .base import *


class HarvestProjectListSerializer(ListSerialMixin, s.ListSerializer):

    DATA_KEY = 'project'


class HarvestProjectModelSerializer(s.ModelSerializer):
    """Provides mapping of Harvest data to our model."""
    id = s.IntegerField(source='harvest_id')
    client_id = s.IntegerField()
    has_project = s.SerializerMethodField()

    def has_project(self, obj):
        return  hasattr(obj, 'project')

    class Meta:
        model = HarvestProject
        exclude = ['uuid', 'harvest_id', 'client', 'harvest_users']


class HarvestProjectSerializer(s.Serializer):
    """For serializing data from the Harvest REST API."""
    project = HarvestProjectModelSerializer()
    class Meta:
        list_serializer_class = HarvestProjectListSerializer
