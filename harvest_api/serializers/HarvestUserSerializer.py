"""
serializers/HarvestUserSerializer.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

from .base import *


class HarvestUserSerializer(s.ModelSerializer):
    id = s.IntegerField(source='harvest_id')

    class Meta:
        model = HarvestUser
        fields = [ 'first_name', 'last_name', 'email', 
            'timezone', 'has_access_to_all_future_projects',
            'default_hourly_rate', 'is_active', 'is_admin',
            'is_contractor', 'department', 'created_at',
            'updated_at'
        ]
        exclude = ['uuid', 'harvest_id']


class HarvestUserAccountSerializer(s.ModelSerializer):
    """Maps in user data from account request."""
    id = s.IntegerField(source='harvest_id')
    admin = s.BooleanField(source='is_admin')

    class Meta:
        model = HarvestUser
        fields = ['first_name', 'last_name', 'email', 
            'timezone', 'timezone_identifier', 'timezone_utc_offset',
            'avatar_url', 'timestamp_timers', 'admin', 'id'
        ]
        # exclude = ['uuid', 'harvest_id', 'telephone', 'user', 
        #     'access_token', 'refresh_token', 'has_auth_harvest',
        #     'last_refreshed_token', 'has_access_to_all_future_projects',
        #     'default_hourly_rate', 'is_active', 'is_admin', 'is_contractor',
        #     'department', 'created_at', 'updated_at', 
        # ]