"""
projects/serializers/base.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Project Status
"""


from rest_framework import serializers as s

from ..models import *


class ListSerialMixin(object):

    DATA_KEY = None

    def create(self, validated_data):
        assert self.DATA_KEY, "Configure the data key for this serializer."
        saved = []
        model = self.child.fields.get(self.DATA_KEY).Meta.model
        for obj_data in validated_data:
            obj_dict = obj_data.get(self.DATA_KEY)
            obj, created = model.objects.update_or_create(harvest_id=obj_dict.get('harvest_id'), defaults=obj_dict)
            saved.append(obj)
        return saved


class HarvestApiError(Exception):
    pass