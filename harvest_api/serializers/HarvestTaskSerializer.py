"""
serializers/HarvestTaskSerializer.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""


from .base import *


class TaskListSerializer(ListSerialMixin, s.ListSerializer):

    DATA_KEY = 'task'


class TaskModelSerializer(s.ModelSerializer):
    id = s.IntegerField(source='harvest_id')

    class Meta:
        model = HarvestTask
        exclude = ['uuid', 'harvest_id']


class HarvestTaskSerializer(s.Serializer):

    task = TaskModelSerializer()
    class Meta:
        list_serializer_class = TaskListSerializer