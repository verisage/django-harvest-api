"""
HarvestProjectViewSet.py

Copyright (c) 2015 Verisage, LLC.

Author J. Harding, 2015
"""


from .base import *
from ..models import HarvestProject
from ..serializers import HarvestProjectModelSerializer

class HarvestProjectViewSet(ModelViewSet):
    queryset = HarvestProject.objects.all()
    serializer_class = HarvestProjectModelSerializer

    def get_queryset(self):
        return self.request.user.harvestuser.harvest_projects.all()