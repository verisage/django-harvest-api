from django.contrib import admin
from .models import *

admin.site.register(HarvestUser)
admin.site.register(HarvestProject)
admin.site.register(HarvestClient)
# Register your models here.
