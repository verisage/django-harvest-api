"""
__init__.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

# from django.conf import settings
from django.template.defaultfilters import urlencode

from harvest import HarvestRestClient

from .HarvestApi import HarvestApi

harvest_client = HarvestRestClient(
    settings.HARVEST_DOMAIN, 
    client_id=settings.HARVEST_CLIENT_ID, 
    redirect_uri=settings.HARVEST_REDIRECT,
)


def get_harvest_oauth_tokens(code):
    """Gets initial oauth tokens from Harvest after authorizing account access."""
    return harvest_client.get_tokens(code, settings.HARVEST_CLIENT_SECRET)

def refresh_harvest_oauth_tokens(refresh_token):
    """Refreshes access token."""
    return harvest_client.refresh_tokens(refresh_token, settings.HARVEST_CLIENT_SECRET)

__all__ = ['get_harvest_oauth_tokens', 'refresh_harvest_oauth_tokens', 'HarvestApi']