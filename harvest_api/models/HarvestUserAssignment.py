"""
HarvestUserAssignment.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015
"""

from .base import *


class HarvestUserAssignment(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)
    
    user = HarvestForeignKey('HarvestUser')
    project = HarvestForeignKey('HarvestProject')

    deactivated = m.BooleanField(default=False)
    hourly_rate = m.DecimalField(max_digits=13, decimal_places=3, default='0.00')
    is_project_manager = m.BooleanField(default=False)
    created_at = m.DateTimeField()
    updated_at = m.DateTimeField()

    class Meta:
        app_label='harvest_api'
