"""
HarvestUser.py

Copyright (c) 2015 Verisage, LLC.

Author J. Harding, 2015

Model to hold data from Harvest.
"""


from phonenumber_field.modelfields import PhoneNumberField


from .base import *


class HarvestUser(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)

    user = m.OneToOneField(settings.AUTH_USER_MODEL, 
        blank=True, null=True,
    )

    #Oauth fields
    access_token = m.CharField(max_length=150, blank=True)
    refresh_token = m.CharField(max_length=150, blank=True)
    has_auth_harvest = m.BooleanField(default=False)
    last_refreshed_token = m.DateTimeField(blank=True, null=True)

    
    telephone = PhoneNumberField(blank=True)
    has_access_to_all_future_projects = m.BooleanField(default=False)
    default_hourly_rate = m.DecimalField(
        max_digits=13, decimal_places=3,
        default='0.00')
    is_active = m.BooleanField(default=True)
    is_admin = m.BooleanField(
        default=False,
        help_text=ugettext_lazy("If user is an admin in Harvest."))
    is_contractor = m.BooleanField(default=False)
    department = m.CharField(
        max_length=20, blank=True)
    created_at = m.DateTimeField(blank=True, null=True)
    updated_at = m.DateTimeField(blank=True, null=True)

    #Fields return in user and account request
    #TODO Verify what fields are returned on a user request and account request.
    first_name = m.CharField(max_length=50)
    last_name = m.CharField(max_length=50)
    email = m.EmailField()
    timezone = m.CharField(
        max_length=50, blank=True
    )

    #Fields returned from the account request
    timezone_utc_offset = m.IntegerField(blank=True)
    timezone_identifier = m.CharField(
        max_length=50, blank=True,
    )
    avatar_url = m.CharField(max_length=150, blank=True)
    timestamp_timers = m.BooleanField(default=False)

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return "%s %s" %(self.first_name, self.last_name)

    class Meta:
        app_label='harvest_api'
