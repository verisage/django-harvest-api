"""
HarvestClient.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Map in client data from Harvest.
"""


from .base import *

class HarvestClient(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    if HARVEST_API_SETTINGS.get('client'):
        client = m.OneToOneField(
            HARVEST_API_SETTINGS.get('client'),
            blank=True, null=True
        )

    harvest_id = m.PositiveIntegerField(unique=True)
    name = m.CharField(max_length=100)
    created_at = m.DateTimeField()
    updated_at = m.DateTimeField()
    highrise_id = m.IntegerField(blank=True, null=True)
    cache_version = m.IntegerField()
    currency = m.CharField(max_length=100)
    active = m.BooleanField(default=True)
    currency_symbol = m.CharField(max_length=5)
    details = m.TextField(max_length=250, blank=True)
    default_invoice_timeframe = m.CharField(
        max_length=100, blank=True, null=True
    )
    last_invoice_kind = m.CharField(
        max_length=50, blank=True, null=True
    )

    def __unicode__(self):
        return self.name

    def __str__(self): 
        return self.__unicode__()

    class Meta:
        app_label='harvest_api'            



