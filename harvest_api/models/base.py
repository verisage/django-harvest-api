"""
models/base.py

Copyright (c) 2015 Verisage, LLC.

Author J. Harding, 2015
"""


import uuid

from django.db import models as m
from django.utils.translation import ugettext_lazy
from django.conf import settings
from django.utils import timezone

from harvest_api.settings import HARVEST_API_SETTINGS


class HarvestForeignKey(m.ForeignKey):
    def __init__(self, *args, **kwargs):
        kwargs.update(to_field='harvest_id')
        super(HarvestForeignKey, self).__init__(*args, **kwargs)
