"""
HarvestTask.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Map in task data from Harvest.
"""

from .base import *

class HarvestTask(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)

    if HARVEST_API_SETTINGS.get('task'):
        task = m.OneToOneField(
            HARVEST_API_SETTINGS.get('task'),
            blank=True, null=True
        )
    
    billable_by_default = m.BooleanField(default=True)
    deactivated = m.BooleanField(default=False)
    default_hourly_rate = m.DecimalField(max_digits=13, decimal_places=2)
    is_default = m.BooleanField(default=True)
    name = m.CharField(max_length=100)
    created_at = m.DateTimeField()
    updated_at = m.DateTimeField()

    class Meta:
        app_label='harvest_api'