"""
HarvestProject.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Map in project data from Harvest.
"""


from .base import *

class HarvestProject(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_users = m.ManyToManyField('HarvestUser', related_name='harvest_projects')

    client = HarvestForeignKey('HarvestClient')

    if HARVEST_API_SETTINGS.get('project'):
        project = m.OneToOneField(
            HARVEST_API_SETTINGS.get('project'),
            blank=True, null=True
        )

    harvest_id = m.PositiveIntegerField(unique=True)
    name = m.CharField(max_length=100)
    code = m.CharField(max_length=50)
    active = m.BooleanField(default=True)
    billable = m.BooleanField(default=True)
    bill_by = m.CharField(max_length=50, blank=True)
    hourly_rate = m.DecimalField(
        max_digits=13, decimal_places=2,
        blank=True, null=True
    )
    budget_by = m.CharField(max_length=50)
    budget = m.DecimalField(
        max_digits=13, decimal_places=2,
        blank=True, null=True
    )
    notify_when_over_budget = m.BooleanField(default=True)
    over_budget_notification_percentage = m.DecimalField(max_digits=13, decimal_places=1)
    over_budget_notified_at = m.CharField(max_length=100, null=True)
    show_budget_to_all = m.BooleanField(default=True)
    created_at = m.DateTimeField()
    updated_at = m.DateTimeField()
    starts_on = m.DateField()
    ends_on = m.DateField(blank=True, null=True)
    hint_earliest_record_at = m.DateField(blank=True, null=True)
    hint_latest_record_at = m.DateField(blank=True, null=True)
    notes = m.TextField(blank=True)
    cost_budget = m.DecimalField(
        max_digits=13, decimal_places=2,
        blank=True, null=True
    )
    cost_budget_include_expenses = m.BooleanField(default=False)

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.__unicode__()

    class Meta:
        app_label='harvest_api'
