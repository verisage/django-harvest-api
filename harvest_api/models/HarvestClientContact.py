"""
HarvestClientContact.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Project Status

Map in contact data from Harvest.  Contacts are nested under clients.
"""

from .base import *
from phonenumber_field.modelfields import PhoneNumberField

class HarvestClientContact(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)

    client = HarvestForeignKey('HarvestClient')
    
    last_name = m.CharField(max_length=100)
    first_name = m.CharField(max_length=100)
    email = m.EmailField()
    phone_mobile = PhoneNumberField(blank=True)
    phone_office = PhoneNumberField(blank=True)
    fax = PhoneNumberField(blank=True)
    created_at = m.DateTimeField()
    title = m.CharField(max_length=100)
    updated_at = m.DateTimeField()
    
    class Meta:
        app_label='harvest_api'