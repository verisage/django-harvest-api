"""
HarvestDayEntry.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Harvest Project Status
"""

from .base import *


class HarvestDayEntry(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)

    user = HarvestForeignKey('HarvestUser')
    project = HarvestForeignKey('HarvestProject')
    task = HarvestForeignKey('HarvestTask')

    hours = m.DecimalField(max_digits=13, decimal_places=3)
    notes = m.TextField(blank=True, null=True)
    spent_at = m.DateField()
    is_billed = m.BooleanField(default=False)
    is_closed = m.BooleanField(default=False)
    created_at = m.DateTimeField(default=timezone.now)
    updated_at = m.DateTimeField(blank=True, null=True)
    timer_started_at = m.DateTimeField()
    started_at = m.CharField(max_length=50)
    ended_at = m.CharField(max_length=50)

    class Meta:
        app_label='harvest_api'