"""
HarvestTaskAssignment.py

Copyright (c) 2015 Verisage, LLC. All rights reserved unless otherwise noted.

Author J. Harding, 2015

Map in task assignment data from Harvest.
"""


from .base import *

class HarvestTaskAssignment(m.Model):
    uuid = m.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    harvest_id = m.PositiveIntegerField(unique=True)

    project = HarvestForeignKey('HarvestProject')
    task = HarvestForeignKey('HarvestTask')
    
    billable = m.BooleanField(default=True)
    deactivated = m.BooleanField(default=False)
    budget = m.DecimalField(max_digits=13, decimal_places=2)
    hourly_rate = m.DecimalField(max_digits=13, decimal_places=2)
    created_at = m.DateTimeField()
    updated_at = m.DateTimeField()

    class Meta:
        app_label='harvest_api'